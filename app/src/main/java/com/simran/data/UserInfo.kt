package com.simran.data

import com.simran.database.entity.TestEntity

object UserInfo {

    var testUser:TestEntity?=null
    fun init(){
        testUser= TestEntity()
    }


    fun getCurrentUser()= testUser

    fun clear(){
        testUser=null
    }
}