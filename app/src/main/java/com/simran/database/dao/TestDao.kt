package com.simran.database.dao


import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.simran.database.entity.TestEntity


@Dao
interface TestDao : BaseDao<TestEntity> {

    @Query("SELECT * FROM TestEntity")
    fun getAllTests(): LiveData<List<TestEntity>>

    @Query("SELECT * FROM TestEntity WHERE TestEntity.name = :UserName")
    fun getTestUser(UserName:String):LiveData<TestEntity>
}