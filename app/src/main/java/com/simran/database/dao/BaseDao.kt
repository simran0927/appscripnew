package com.simran.database.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update


interface BaseDao<in T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(data: List<T>)

    @Update
    fun update(data: T)

    @Delete
    fun delete(data: T)

    @Delete
    fun deleteMultiple(data: List<T>)

    @Delete
    fun deleteList(data: List<T>)
}