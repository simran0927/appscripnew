package com.simran.database.entity


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity()
data class TestEntity(
    @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "name") var name: String="",
    @ColumnInfo(name = "time") var time: Long?=null,
    @ColumnInfo(name = "firstAnswer")  var firstAnswer: String="",
    @ColumnInfo(name = "secondAnswer")  var secondAnswer:String=""
)