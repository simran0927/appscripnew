package com.simran.repository

import androidx.lifecycle.LiveData
import com.simran.database.TestDatabase
import com.simran.database.entity.TestEntity
import com.simran.util.extension.workerRE


class LocalRepository {

    private val mDatabase: TestDatabase? by lazy { TestDatabase.getInstance() }


    fun insert(model: TestEntity){

        workerRE {
            mDatabase?.testDao()?.insert(model)
        }
    }

    fun update(model: TestEntity) {
        workerRE {
            mDatabase?.testDao()?.update(model)
        }
    }

    fun delete(model: TestEntity) {
        workerRE {
            mDatabase?.testDao()?.delete(model)
        }
    }

    fun getAllTests(): LiveData<List<TestEntity>>? {
        return mDatabase?.testDao()?.getAllTests()
    }

    fun getCurrentTestUser(userName:String):LiveData<TestEntity>?{
        return mDatabase?.testDao()?.getTestUser(userName)
    }
}