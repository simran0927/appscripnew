package com.simran.util.extension

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Handler
import androidx.core.app.ActivityOptionsCompat
import java.text.SimpleDateFormat
import java.util.*


////////// delay ///////////////

inline fun Context.delay(timeMS: Long, crossinline body: () -> Unit): Handler {
    return Handler().apply {
        postDelayed({
            body()
        }, timeMS)
    }
}

inline fun <reified T> Activity.openActivity(extras: Intent.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    intent.extras()
    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this)
    startActivity(intent, options.toBundle())
}


fun Long.toTimeFormat(dateFormat: String = "yyyy-MM-dd HH:mm"): String {

    val date = Date()
    date.time = this
    val toformat = SimpleDateFormat(dateFormat, Locale("en"))
    return toformat.format(date)
}