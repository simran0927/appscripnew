package com.simran.ui.viewmodel

import androidx.lifecycle.LiveData
import com.simran.app.getPref
import com.simran.base.BaseViewModel
import com.simran.data.Keys
import com.simran.database.entity.TestEntity

class TestViewmodel : BaseViewModel() {

    private val testLd by lazy { getLocalRepository().getAllTests() }
private val testCurreenUserLd by lazy { getLocalRepository().getCurrentTestUser(getPref().get(Keys.USERNAME,"")) }
    val currentObserver get() = testCurreenUserLd
    val testAllObserver get() = testLd
    fun getAllTests(): LiveData<List<TestEntity>>? {
        return testLd
    }

    fun getCurrentUser():LiveData<TestEntity>?{
        return testCurreenUserLd
    }


}