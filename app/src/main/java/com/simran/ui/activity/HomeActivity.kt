package com.simran.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.simran.R
import com.simran.base.BaseActivity
import com.simran.ui.fragment.NameFragment

class HomeActivity : BaseActivity(R.id.home_container) {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        replaceFragment(NameFragment(),true)
    }
}
