package com.simran.ui.activity

import android.os.Bundle
import com.simran.R
import com.simran.base.BaseActivity
import com.simran.util.extension.delay
import com.simran.util.extension.openActivity

class SplashActivity :BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initViews()
    }


    private fun initViews(){
        delay(3000) {
            finish()
            openActivity<HomeActivity>()
        }
    }
}
