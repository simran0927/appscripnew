package com.simran.ui.fragment

import android.os.Bundle
import android.view.View
import com.simran.R
import com.simran.base.BaseFragment
import com.simran.data.UserInfo
import com.simran.repository.LocalRepository
import com.simran.util.extension.toast
import kotlinx.android.synthetic.main.fragment_select_multiple.*
import kotlinx.android.synthetic.main.fragment_select_one.*
import kotlinx.android.synthetic.main.fragment_select_one.next
import org.koin.android.ext.android.inject

class SelectMultipleFragment:BaseFragment(R.layout.fragment_select_multiple) {

    var SecondAnswers:String?=null
    val localRepository by inject<LocalRepository>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
setCheckBoxValue()
        setUpClick()
    }

    private fun setUpClick(){
       next.setOnClickListener {
           if(validateIsSelected()){

               SecondAnswers?.let {
                   UserInfo.getCurrentUser()?.secondAnswer=it
               }
               UserInfo.getCurrentUser()?.time=System.currentTimeMillis()
               UserInfo.getCurrentUser()?.let { it -> localRepository.insert(it)
               UserInfo.clear()
               }
               replaceFragment(R.id.multiple_container,SummaryFragment(),true)
       }
       }

    }


 private fun validateIsSelected():Boolean{
     return if(white_text.isChecked || yellow.isChecked || orange.isChecked || green.isChecked){
         true
     }else{
         toast("Choose any one value")
         false
     }
 }

    private fun checkString(Value:String){
        if(SecondAnswers==null){
            SecondAnswers=Value
        }else{
            SecondAnswers+= ",$Value"
        }
    }

    private fun setCheckBoxValue(){
        /*when {
            white_text.isChecked -> checkString(white_text.text.toString())
            yellow.isChecked -> checkString(yellow.text.toString())
            orange.isChecked -> checkString(orange.text.toString())
            green.isChecked -> checkString(green.text.toString())
        }
*/
        white_text.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                checkString(white_text.text.toString())
            }

        }
        yellow.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                checkString(yellow.text.toString())
            }
        }

        orange.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                checkString(orange.text.toString())
            }
        }
        green.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                checkString(green.text.toString())
            }
        }
    }

}