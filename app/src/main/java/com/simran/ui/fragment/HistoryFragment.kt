package com.simran.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.simran.R
import com.simran.base.BaseFragment
import com.simran.database.entity.TestEntity
import com.simran.ui.adapter.ItemsAdapter
import com.simran.ui.viewmodel.TestViewmodel
import kotlinx.android.synthetic.main.fragment_history.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HistoryFragment:BaseFragment(R.layout.fragment_history) {
    lateinit var itemsAdapter: ItemsAdapter
    val viewModel by viewModel<TestViewmodel>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
observeData()
    }


    private fun observeData(){
     viewModel.testAllObserver?.observe(this, Observer {
itemsAdapter= ItemsAdapter(it as ArrayList<TestEntity>,activity!!)
         history_rv.adapter=itemsAdapter
     })
    }
}