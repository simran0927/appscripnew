package com.simran.ui.fragment

import android.os.Bundle
import android.view.View
import com.simran.R
import com.simran.app.getPref
import com.simran.base.BaseFragment
import com.simran.data.Keys
import com.simran.data.UserInfo
import com.simran.util.extension.toast
import kotlinx.android.synthetic.main.fragment_add_name.*

class NameFragment:BaseFragment(R.layout.fragment_add_name) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        UserInfo.init()
      setUpClick()
    }

    private fun setUpClick(){
       next.setOnClickListener {
           if(validateData()){
getPref().set(Keys.USERNAME,name_text.text.toString())
           UserInfo.getCurrentUser()?.name= name_text.text.toString()
           replaceFragment(R.id.name_conatiner,SelectOneFragment(),true)
       }}
    }



    //// check whether name is entered or not
    private fun validateData():Boolean{
        return if(name_text.text!= null && name_text.text.isNotBlank()){
            true
        }else{
            toast("Please enter your name first.")
            false
        }
    }
}