package com.simran.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.simran.R
import com.simran.app.getPref
import com.simran.base.BaseFragment
import com.simran.ui.activity.HomeActivity
import com.simran.ui.viewmodel.TestViewmodel
import com.simran.util.extension.openActivity
import kotlinx.android.synthetic.main.fragment_summary.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SummaryFragment:BaseFragment(R.layout.fragment_summary) {
    private val viewModel by viewModel<TestViewmodel>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        setUpClick()
    }


    private fun observeData(){
     viewModel.currentObserver?.observe(this, Observer {
name_text.text=resources.getString(R.string.hello_1_s,it.name)
         reply_best_cricketer.text=resources.getString(R.string.answer_1_s,it.firstAnswer)
         reply_flag_colors.text=resources.getString(R.string.answers_1_s,it.secondAnswer)
     })
    }

    private fun setUpClick(){
        finish.setOnClickListener {
            getPref().clearAll()
            openActivity<HomeActivity>()
        }

        history.setOnClickListener {
            replaceFragment(R.id.summary_container,HistoryFragment(),true)
        }
    }

}