package com.simran.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import com.simran.R
import com.simran.base.BaseFragment
import com.simran.data.UserInfo
import kotlinx.android.synthetic.main.fragment_select_one.*

class SelectOneFragment :BaseFragment(R.layout.fragment_select_one){

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    setCricketerValue()
        setUpClick()
    }

    private fun setUpClick(){
        next.setOnClickListener {
           if(validateCheckedItem()){
               replaceFragment(R.id.select_one_container,SelectMultipleFragment(),true)
             }


           }
        }



    private fun setCricketerValue(){
        choose_one.setOnCheckedChangeListener { group, checkedId ->

            when (checkedId) {
                R.id.sachin_text -> {
                    UserInfo.getCurrentUser()?.firstAnswer=sachin_text.text.toString()
                }
                R.id.virat -> {
                    UserInfo.getCurrentUser()?.firstAnswer=virat.text.toString()
                }
                R.id.jacques -> {
                    UserInfo.getCurrentUser()?.firstAnswer=jacques.text.toString()
                }
                R.id.adam -> {
                    UserInfo.getCurrentUser()?.firstAnswer=adam.text.toString()
                }
            }
    }
    }

    private fun validateCheckedItem():Boolean{
        return choose_one.checkedRadioButtonId!=-1
    }
}