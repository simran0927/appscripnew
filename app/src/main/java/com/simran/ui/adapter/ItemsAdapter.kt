package com.simran.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.simran.R
import com.simran.database.entity.TestEntity
import com.simran.util.extension.toTimeFormat
import kotlinx.android.synthetic.main.items_history.view.*

class ItemsAdapter(val testEntityList: ArrayList<TestEntity>, val context: Context) :
    RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.items_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return testEntityList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.game.text = context.resources.getString(
            R.string.game_1_s,
            testEntityList[position].time?.toTimeFormat()
        )
        holder.itemView.name_text.text =
            context.resources.getString(R.string.name_1_s, testEntityList[position].name)
        holder.itemView.reply_best_cricketer.text =
            context.resources.getString(R.string.answer_1_s, testEntityList[position].firstAnswer)
        holder.itemView.reply_flag_colors.text =
            context.resources.getString(R.string.answers_1_s, testEntityList[position].secondAnswer)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}