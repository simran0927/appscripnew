package com.simran.base

import androidx.lifecycle.ViewModel
import com.simran.repository.LocalRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

open class BaseViewModel : ViewModel(), KoinComponent {
    private val localRepo by inject<LocalRepository>()


    fun getLocalRepository(): LocalRepository {
        return localRepo
    }
}