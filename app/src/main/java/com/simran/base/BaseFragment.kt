package com.simran.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

open class BaseFragment(@LayoutRes val view: Int = 0) : Fragment() {


    private val childFm by lazy { childFragmentManager }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(view, container, false)
    }

    fun addFragment(containerId: Int, fragment: Fragment, addToBackStack: Boolean) {
        val transaction = childFm.beginTransaction()
        transaction.add(containerId, fragment, fragment::class.java.simpleName)
        transaction.addToBackStack(if (addToBackStack) "Added" else null)
        transaction.commitAllowingStateLoss()
    }

    fun replaceFragment(containerId: Int, fragment: Fragment, addToBackStack: Boolean) {
        val transaction = childFm.beginTransaction()
        transaction.replace(containerId, fragment, fragment::class.java.simpleName)
        transaction.addToBackStack(if (addToBackStack) fragment.tag else null)
        transaction.commitAllowingStateLoss()
    }

    fun addFragmentToActivity(fragment: Fragment, addToBackStack: Boolean) {
        (activity as? BaseActivity)?.addFragment(fragment, addToBackStack)
    }

    fun replaceFragmentToActivity(fragment: Fragment, addToBackStack: Boolean) {
        (activity as? BaseActivity)?.replaceFragment(fragment, addToBackStack)
    }
}